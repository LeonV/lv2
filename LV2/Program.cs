﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1:
            //DiceRoller roller = new DiceRoller();
            //for (int i = 0; i < 20; i++)
            //{
            //    Die tempDie = new Die(6);
            //    roller.InsertDie(tempDie);
            //}
            //roller.RollAllDice();
            //foreach (int result in roller.GetRollingResults()) {
            //    Console.WriteLine();
            //}
            //Zadatak 2:
            Random random = new Random();
            DiceRoller roller = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                Die tempDie = new Die(6, random);
                roller.InsertDie(tempDie);
            }
            roller.RollAllDice();
            foreach (int result in roller.GetRollingResults())
            {
                Console.WriteLine(result);
            }
        }
    }
}
