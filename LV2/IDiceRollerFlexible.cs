﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    interface IDiceRollerFlexible
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
