﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        //private Logger logger;
        private ILogger ilogger;
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.ilogger = new ConsoleLogger();
        }
        public DiceRoller(string filePath)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.ilogger = new FileLogger(filePath);
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        //Zadatak 4:
        //public void LogRollingResults()
        //{
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        string temp = "";
        //        foreach (int result in this.resultForEachRoll)
        //        {
        //            temp = temp + System.Environment.NewLine + result.ToString();
        //        }
        //        ilogger.Log(temp);
        //    }
        //}
        public string GetStringRepresentation()
        {
            string temp = "";
            foreach (int result in this.resultForEachRoll)
            {
                temp = temp + System.Environment.NewLine + result.ToString();
            }
            return temp;
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void LogRollingResults()
        {
            ilogger.Log(this);
        }
    }
}
